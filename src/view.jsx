// This file is part of GMS-Training.
//
// GMS-Training is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// GMS-Training is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with GMS-Training.  If not, see <https://www.gnu.org/licenses/>.

import { h } from "preact";

const Navigate = (to) => ({type: "Navigate", to});

const SetInput = (input) => ({type: "SetInput", input});
const SubmitInput = (input) => ({type: "SubmitInput", input});
const PopLine = (line) => ({type: "PopLine", line});

const Home = ({state}) => (
    <article>
      <header>
        <h1>Gegevens invoeren</h1>
      </header>
      <p>Vanuit je kladblok kun je de verschllende velden invullen mbv speciale voorvoegsels.</p>
      <dl class="key-bindings">
        { state.commands.map(
            (cmd) => [
                <dt>{cmd.prefix}</dt>,
                <dd>
                  {cmd.name}
                  <span class="example">{cmd.pattern}</span>
                </dd>
            ]
        )}
      </dl>
    </article>
);

const Basics = ({state, input}) => {
    const handleInput = (e) => input((e.keyCode === 13 ? SubmitInput : SetInput) (e.target.value));

    const command = state.input.command;
    return (
        <article class="basics">
          <div class="record">
            <dl>
              <span class="heading">Melder</span>
              <dt>Naam</dt>
              <dd>{ state.record.reporter.name }</dd>
              <dt>Geslacht</dt>
              <dd>{ state.record.reporter.gender }</dd>
              <dt>Geboortedatum</dt>
              <dd>{ state.record.reporter.birthdate }</dd>

              <span class="heading">Patient</span>
              <dt>Naam</dt>
              <dd>{ state.record.patient.name }</dd>
              <dt>Geslacht</dt>
              <dd>{ state.record.patient.gender }</dd>
              <dt>Geboortedatum</dt>
              <dd>{ state.record.patient.birthdate }</dd>
            </dl>
            <dl>
              <span class="heading">Haal adres</span>
              <dt>Straat</dt>
              <dd>{ state.record.fromAddress.street }</dd>
              <dt>Plaats</dt>
              <dd>{ state.record.fromAddress.city }</dd>

              <span class="heading">Breng adres</span>
              <dt>Object</dt>
              <dd>{ state.record.toAddress.object} </dd>
              <dt>Straat</dt>
              <dd>{ state.record.toAddress.street }</dd>
              <dt>Plaats</dt>
              <dd>{ state.record.toAddress.city }</dd>
            </dl>
          </div>

          <div class="col-side">
            <dl class="command-info">
              <dt>Veld</dt>
              <dd>{ command ? command.name : "—" }</dd>

              <dt>Voorvoegsel</dt>
              <dd>{ command ? command.prefix : "—" }</dd>

              <dt>Patroon</dt>
              <dd>{ command ? command.pattern : "—" }</dd>

              <dt>Invoer</dt>
              <dd>{ state.input.value || "—" }</dd>
            </dl>
          </div>

          <div class="intellipad">
            <input type="text"
                   value={state.input.line}
                   autofocus
                   onkeyup={handleInput} />
          </div>

          <div class="scratchpad">
            <ul class="lines">
              { state.scratchpad.map(
                  (line) => <li class="line" onclick={() => input(PopLine(line))}>{ line }</li>
              ) }
            </ul>
          </div>
        </article>
    );
};

export const TrainingApp = ({state, input}) => (
    <div class="page">
      <header>
        <h1 class="action" onclick={() => input(Navigate('Home'))}>GMS Training</h1>
        <nav>
          <ul>
            <li><a href="#" onclick={() => input(Navigate('Home'))}>Uitleg</a></li>
            <li><a href="#" onclick={() => input(Navigate('Basics'))}>Basis</a></li>
          </ul>
        </nav>
      </header>
      <main>
        { state.content === 'Home' && <Home state={state} /> }
        { state.content === 'Basics' && <Basics state={state} input={input} /> }
      </main>
      <footer>
        <div class="content">
          <p>Een training tool voor <a href="https://nl.wikipedia.org/wiki/Ge%C3%AFntegreerd_Meldkamer_Systeem">GMS</a>.</p>
          <ul>
            <li>Licentie <a href="https://www.gnu.org/licenses/gpl-3.0">GPLv3</a></li>
            <li><a href="https://gitlab.com/bjpbakker/gms-training">Source code</a></li>
          </ul>
        </div>
      </footer>
    </div>
);
