// This file is part of GMS-Training.
//
// GMS-Training is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// GMS-Training is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with GMS-Training.  If not, see <https://www.gnu.org/licenses/>.

import { compose, constant, flip, id, uncurry } from "@fpjs/overture/base";
import { concat } from "@fpjs/overture/algebras/semigroup";
import { over, set, setAll, Record } from "@fpjs/overture/control/lens";
import { Identity } from "@fpjs/overture/data/identity";
const _ = Record;

import { patchBuiltins } from "@fpjs/overture/patches";

import { h, render} from "preact";

import { App } from "@fpjs/signal";
const { Eff, start, noEffects, transition } = App;

import { TrainingApp } from "./view";

const emptyMention = {
    fromAddress: {
        city: '',
        street: '',
    },
    toAddress: {
        city: '',
        street: '',
        object: '',
    },
    reporter: {
        name: '',
        gender: '',
        birthdate: '',
    },
    patient: {
        name: '',
        gender: '',
        birthdate: '',
    },
};

const initState = {
    content: 'Home',
    commands: [
        {
            key: 'from-address',
            name: 'Haal adres',
            pattern: '<plaatsnaam>/<straatnaam>',
            prefix: ':=',
        },
        {
            key: 'to-address',
            name: 'Breng adres',
            pattern: '<plaatsnaam>/<straatnaam>',
            prefix: 'b=',
        },
        {
            key: 'to-object',
            name: 'Breng object',
            pattern: '<tekst>',
            prefix: 'b;',
        },
        {
            key: 'reporter',
            name: 'Melder',
            pattern: '<naam>;<m|v>;ddmmyyyy',
            prefix: 'm\\'
        },
        {
            key: 'patient',
            name: 'Patient',
            pattern: '<naam>;<m|v>;ddmmyyyy',
            prefix: 'p\\'
        },
    ],
    scratchpad: [],
    record: emptyMention,
    input: {
        line: '',
        command: null,
        value: '',
    },
};

const remove = (x) => (xs) => {
    const idx = xs.indexOf(x);
    if (~idx) {
        return [...xs.slice(0, idx), ...xs.slice(idx + 1)];
    }
    return xs;
};

const handleInput = (input) => (state) => {
    const match = state.commands.find((x) => input.startsWith(x.prefix));
    return setAll (
        [_.input.line, input],
        [_.input.command, match],
        [_.input.value, match ? input.substring(match.prefix.length) : '']
    ) (state);
};

const processInput = ({command, value}) => {
    if (command == null) {
        return id;
    }

    switch (command.key) {
    case 'to-address': {
        const [city, street] = value.split('/');
        return setAll ([_.toAddress.city, city], [_.toAddress.street, street]);
    }
    case 'to-object': {
        return set (_.toAddress.object) (value);
    }
    case 'from-address': {
        const [city, street] = value.split('/');
        return set (_.fromAddress) ({city, street});
    }
    case 'patient': {
        const [name, gender, birthdate] = value.split(';');
        return set (_.patient) ({name, gender, birthdate});
    }
    case 'reporter': {
        const [name, gender, birthdate] = value.split(';');
        return set (_.reporter) ({name, gender, birthdate});
    }
    default:
        return id;
    }
};

const foldp = (event) => {
    switch (event.type) {
    case 'Navigate':
        return transition (set (_.content) (event.to));
    case 'SetInput':
        return transition (handleInput (event.input));
    case 'SubmitInput':
        return transition ([
            (set (_.input) ({line: '', command: null, value: ''})),
            (over (_.scratchpad) (flip (concat) ([event.input]))),
            (state) => over (_.record) (processInput (state.input)) (state),
        ].reduce(uncurry(compose), id));
    case 'PopLine':
        return transition (
            compose (handleInput (event.line))
                    (over (_.scratchpad) (remove (event.line)))
        );
    default:
        return noEffects;
    }
};

const run = (root) => {
    const config = {
        initState,
        foldp,
        render: Eff(({state, input}) => render(<TrainingApp state={state} input={input} />, root)),
        inputs: []
    };

    start (config);
};

const main = () => {
    const root = document.getElementById("app");
    if (root != null) {
        run(root);
    } else {
        console.log("Waiting for app root to render..");
        setTimeout(main, 10);
    }
};

patchBuiltins ();
main ();
